#include <iostream>
#include <libpq-fe.h>
using namespace std;

int main()
{
  PGconn *connexion;
  connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=a.nardeux user=a.nardeux");

  if(PQstatus(connexion) == CONNECTION_OK)
  {
    cout << "Connexion réussie !" << std::endl;
  }
  else
  {
    cout << "Echec de connexion !" << std::endl;
  }

  return 0;
}

